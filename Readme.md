# File Upload App

This application is a simple file upload system using React for the frontend and Express for the backend. It allows users to upload files to the server, which then stores them in a local directory. Users can also view a list of uploaded files.

## Project Structure

- `client/`: Contains the React frontend application.
- `server.js`: The Express server application file.
- `uploads/`: Directory where uploaded files are stored.
- `node_modules/`: Directory containing all the project's npm dependencies.
- `package.json` & `package-lock.json`: NPM configuration files which store information about the project and the versions of packages it depends on.
- `Readme.md`: This file, which provides documentation for the project.

## Setup

To get the project running, follow these steps:

### Prerequisites

- Node.js installed (https://nodejs.org/)
- NPM (Node Package Manager) installed with Node.js

### Installation

1. Clone the repository to your local machine:

2. Install dependencies for server: 

npm install

3. Navigate to the client directory and install dependencies for the React application:

cd client
npm install


### Running the Application

You can run the server and client simultaneously in development mode using the following command from the root directory:

```bash
npm run dev

### RUN APP
This command uses concurrently to launch both the React development server and the Express server. The React application will be available at http://localhost:3000 and the Express server will run on http://localhost:3001.



